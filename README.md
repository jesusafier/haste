<h1>Haste</h1>

An **experimental** performance enhancement tweak modules for FoundryVTT

it has 2 features:

1. Wall fix - patches the FOW LOS FOV calculation to be much more efficient using spatial grid calculations. 
    Big words that mean performance is basically unchanged when running 4000+ walls and 50+ light sources on large scenes.
    **For 0.6.x Only, should be disabled on 0.7.x**
    
2. Adaptive GPU FPS - Stops the canvas redraws when no changes occurred in the scene, reduces GPU usage to 0% unless a token moves or the canvas is panned. 
    Best used when disabling cursor pointers. When using this options you can set the FPS limiter to 60fps.
    
**Warnings**

Being Fast is nice, but sometimes *Haste Makes Waste*

Wall fix might reveal walls and cause light leaks, currently known remaining bug is with terrain walls. 
All other bugs have been fixed. As a good practice, use a player token on the map to check all walls are working correctly.


Adaptive GPU won't work well with some module like weather effects. World building is also less supported when using this options.
Some placing of measurements might appear to get stuck sometimes. Future versions will try to fix this.

![](https://i.imgur.com/NQfcIyl.mp4)


<h1>Install</h1>
https://gitlab.com/jesusafier/haste/-/jobs/artifacts/master/raw/module.json?job=build-module

Join the discussions on our [Discord](https://discord.gg/467HAfZ)
and if you liked it, consider supporting me on [patreon](https://www.patreon.com/foundry_grape_juice)