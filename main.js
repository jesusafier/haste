Hooks.once('init', async function() {
    game.settings.register('Haste', 'enabledWall', {
        name: 'Fix CPU on token movement (Walls) (Reload required)',
        default: true,
        type: Boolean,
        scope: 'client',
        config: true,
        hint: 'Enables tweaks that fix slow and cpu intensive fog of war calculations, tokens will move at the same speed in huge and small maps. (Verify with a player token that no light leaks exist)'
    });
    game.settings.register('Haste', 'enabledGPU', {
        name: 'Experimental Adaptive GPU fps tweak (Reload required)',
        default: false,
        type: Boolean,
        scope: 'client',
        config: true,
        hint: 'Reduces GPU usage to draw only when actual changes happen. (Not compatible with some modules)'
    });
    console.log('Hasted!!!');

    if (game.settings.get('Haste', 'enabledWall')) {
        WallsLayer.prototype.initialize = (function() {
            return function() {
                this._lastActive = null;
                this._getVisionEndpoints();
                this._genBuckets();
            }
        })();

        WallsLayer.prototype._genBuckets = (function() {
            return function() {

                var rad = canvas.dimensions.size * 2;

                var buckX = Math.floor(canvas.dimensions.width / rad);
                var buckY = Math.floor(canvas.dimensions.height / rad);
                var wall_buckets = [];

                for (var i = 0; i < buckX; i++) {
                    for (var j = 0; j < buckY; j++) {
                        for (let w of this.objects.children) {
                            let minAx = Math.min(w.data.c[0], w.data.c[2])
                            let minAy = Math.min(w.data.c[1], w.data.c[3])
                            let maxAx = Math.max(w.data.c[0], w.data.c[2])
                            let maxAy = Math.max(w.data.c[1], w.data.c[3])

                            let minBx = i * rad
                            let minBy = j * rad
                            let maxBx = minBx + rad
                            let maxBy = minBy + rad

                            let aLeftOfB = maxAx < minBx;
                            let aRightOfB = minAx > maxBx;
                            let aAboveB = minAy > maxBy;
                            let aBelowB = maxAy < minBy;

                            if (!(aLeftOfB || aRightOfB || aAboveB || aBelowB)) {
                                //do you intersect                     
                                if (wall_buckets[i] == null) {
                                    wall_buckets[i] = new Array();
                                }
                                if (wall_buckets[i][j] == null) {
                                    wall_buckets[i][j] = new Array();
                                }
                                wall_buckets[i][j].push(w);
                            }
                        }
                    }
                }
                this.bucketss = wall_buckets;
            }

        })();

        WallsLayer.prototype.checkCollision = (function(ray) {
            return function(ray) {

                // let bucket_count = Math.ceil((ray.distance*2)/(canvas.dimensions.size*2));

                let minbx = Math.min(Math.floor(ray.A.x / (canvas.dimensions.size * 2)), Math.floor(ray.B.x / (canvas.dimensions.size * 2)));
                let minby = Math.min(Math.floor(ray.A.y / (canvas.dimensions.size * 2)), Math.floor(ray.B.y / (canvas.dimensions.size * 2)));

                let maxbx = Math.max(Math.floor(ray.A.x / (canvas.dimensions.size * 2)), Math.floor(ray.B.x / (canvas.dimensions.size * 2)));
                let maxby = Math.max(Math.floor(ray.A.y / (canvas.dimensions.size * 2)), Math.floor(ray.B.y / (canvas.dimensions.size * 2)));

                walls = new Set(canvas.walls.bucketss.slice(minbx - 1, maxbx + 1).map(function(row) {
                    return row.slice(minby - 1, maxby + 1);
                }).flat(2).filter(w => w && w.data.move !== CONST.WALL_MOVEMENT_TYPES.NONE && !((w.data.door > WALL_DOOR_TYPES.NONE) && (w.data.ds === WALL_DOOR_STATES.OPEN))));
                if (!walls.size)
                    return false;

                // Never allow movement out of bounds
                if (!canvas.grid.hitArea.contains(ray.B.x, ray.B.y))
                    return true;

                // Test wall collision
                return this.constructor.getWallCollisionsForRay(ray, walls, {
                    mode: "any"
                });
            };
        })();

        WallsLayer.getWallCollisionsForRay = (function(ray, walls, {
            mode = "all"
        } = {}) {
            const cached = WallsLayer.getWallCollisionsForRay;
            return function(ray, walls, {
                mode = "all"
            } = {}) {
                // Establish initial data
                const collisions = {};
                const isAny = mode === "any";
                const bounds = [ray.angle - (Math.PI / 2), ray.angle + (Math.PI / 2)];

                // Iterate over provided walls
                for (let w of walls) {

                    // Skip open doors
                    if ((w.data.door > WALL_DOOR_TYPES.NONE) && (w.data.ds === WALL_DOOR_STATES.OPEN))
                        continue;

                    // Skip directional walls where the ray angle is not in the same hemisphere as the wall direction
                    if (w.direction !== null) {
                        if (!w.isDirectionBetweenAngles(...bounds))
                            continue;
                    }

                    // Test for intersections
                    let i = ray.intersectSegment(w.coords);
                    if (i && i.t0 > 0) {
                        if (isAny)
                            return true;
                        i.x = Math.round(i.x);
                        i.y = Math.round(i.y);

                        // Ensure uniqueness of the collision point
                        let pt = `${i.x}.${i.y}`;
                        const c = collisions[pt];
                        if (c) {
                            c.sense = Math.min(w.data.sense, c.sense);
                        } else {
                            i.sense = w.data.sense;
                            collisions[pt] = i;
                        }
                    }
                }

                //

                // Return results
                if (isAny)
                    return false;
                if (mode === "closest")
                    return this._getClosestCollisionPoint(ray, Object.values(collisions));
                return Object.values(collisions);
                return cached.apply(this, arguments);
            };
        })();

        // SightLayer.prototype.getWallsByBuckets = (function(radius, x, y) {

        //     return function(radius, x, y) {
        //         let bucket_count = Math.ceil(radius / (canvas.dimensions.size * 2));
        //         let bx = Math.floor(x / (canvas.dimensions.size * 2));
        //         let by = Math.floor(y / (canvas.dimensions.size * 2));
        //         return canvas.walls.bucketss.slice(bx - bucket_count, bx + bucket_count).map(function(row) {
        //             return row.slice(by - bucket_count, by + bucket_count);
        //         }).flat(2).filter(w=>w && w.data.sense !== CONST.WALL_SENSE_TYPES.NONE);
        //     }
        //     ;
        // }
        // )();

        // SightLayer.prototype.initializeLights = (function({defer=false}={}) {

        //     return function({defer=false}={}) {
        //         const walls = canvas.walls.blockVision;
        //         let light_walls = [];
        //         canvas.lighting.placeables.forEach(l=>this.updateLight(l, {
        //             walls,
        //             defer: true
        //         }));

        //         for (let l of canvas.lighting.placeables) {

        //             light_walls = light_walls.concat(this.getWallsByBuckets((Math.ceil(l.data.dim) + Math.ceil(l.data.bright)), l.data.x, l.data.y));
        //         }

        //         canvas.lighting.light_walls = Array.from(new Set(light_walls));

        //         if (!defer)
        //             this.update();
        //     }
        //     ;
        // }
        // )();

        // function getWallsByBuckets(radius, x, y) {
        //     let bucket_count = Math.ceil(radius / (canvas.dimensions.size * 2));
        //     let bx = Math.floor(x / (canvas.dimensions.size * 2));
        //     let by = Math.floor(y / (canvas.dimensions.size * 2));
        //     return canvas.walls.bucketss.slice(bx - bucket_count, bx + bucket_count).map(function(row) {
        //         return row.slice(by - bucket_count, by + bucket_count);
        //     }).flat(2).filter(w=>w && w.data.sense !== CONST.WALL_SENSE_TYPES.NONE);
        // }

        // function grid_intersect2(r) {
        //   let grid_size = (canvas.dimensions.size * 2);
        //   let buckX = Math.floor(canvas.dimensions.width / grid_size);
        //   let buckY = Math.floor(canvas.dimensions.height / grid_size);

        //   let x0= Math.floor(r.A.x / grid_size);
        //   let y0 =Math.floor(r.A.y / grid_size);
        //   let x1 =Math.floor(r.B.x / grid_size);
        //   let y1 =Math.floor(r.B.y / grid_size);

        //   var dx = Math.abs(x1 - x0);
        //   var dy = Math.abs(y1 - y0);
        //   var sx = (x0 < x1) ? 1 : -1;
        //   var sy = (y0 < y1) ? 1 : -1;
        //   var err = dx - dy;
        //     let walls = [];

        //   while(true) {

        //      try {
        //             if ((x0 >= 0) && (x0 <= buckX) && (y0 >= 0) && (y0 <= buckY)) {
        //               if (canvas.walls.bucketss[x0] != null  && canvas.walls.bucketss[x0][y0] != null)
        //               {  
        //                 walls = [].concat(walls, canvas.walls.bucketss[x0][y0])
        //                 // if (walls.length != 0){

        //                 //                         let collision = WallsLayer.getWallCollisionsForRay(r, walls, {
        //                 //                             mode: "closest"
        //                 //                         });

        //                 //                         if ((collision.x != r.B.x) || (collision.y != r.B.y) || (collision.t0 != 1) || (collision.t1 != 0)) {
        //                 //                             return collision;
        //                 //                         }
        //                 //                         else{
        //                 //                           //console.log('no');
        //                 //                         }
        //                 //                       }
        //                                     }
        //             }
        //         } catch {
        //         }

        //     if ((x0 === x1) && (y0 === y1)) break;
        //     var e2 = 2*err;
        //     if (e2 > -dy) { err -= dy; x0  += sx; }
        //     if (e2 < dx) { err += dx; y0  += sy; }
        //   }

        //   return collision = WallsLayer.getWallCollisionsForRay(r, [...new Set(walls)].filter(w => w), {mode: "closest"});

        // }

        function grid_intersect(r) {

            let grid_size = (canvas.dimensions.size * 2);
            let start = {
                x: r.A.x / grid_size,
                y: r.A.y / grid_size
            };
            let end = {
                x: r.B.x / grid_size,
                y: r.B.y / grid_size
            };

            //Grid cells are 1.0 X 1.0.
            let x = Math.floor(start.x);
            let y = Math.floor(start.y);
            let diffX = end.x - start.x;
            let diffY = end.y - start.y;
            let stepX = Math.sign(diffX);
            let stepY = Math.sign(diffY);

            //Ray/Slope related maths.
            //Straight distance to the first vertical grid boundary.
            let xOffset = end.x > start.x ? (Math.ceil(start.x) - start.x) : (start.x - Math.floor(start.x));
            //Straight distance to the first horizontal grid boundary.
            let yOffset = end.y > start.y ? (Math.ceil(start.y) - start.y) : (start.y - Math.floor(start.y));
            //Angle of ray/slope.
            let angle = Math.atan2(-diffY, diffX);
            //NOTE: These can be divide by 0's, but JS just yields Infinity! :)
            //How far to move along the ray to cross the first vertical grid cell boundary.
            let tMaxX = xOffset / Math.cos(angle);
            //How far to move along the ray to cross the first horizontal grid cell boundary.
            let tMaxY = yOffset / Math.sin(angle);
            //How far to move along the ray to move horizontally 1 grid cell.
            let tDeltaX = 1.0 / Math.cos(angle);
            //How far to move along the ray to move vertically 1 grid cell.
            let tDeltaY = 1.0 / Math.sin(angle);

            //Travel one grid cell at a time.
            let manhattanDistance = Math.abs(Math.floor(end.x) - Math.floor(start.x)) + Math.abs(Math.floor(end.y) - Math.floor(start.y));
            let walls = []
            var buckX = Math.floor(canvas.dimensions.width / grid_size);
            var buckY = Math.floor(canvas.dimensions.height / grid_size);
            let check_cols = false;

            let finish_y = false;
            for (let t = 0; t <= manhattanDistance; ++t) {
                // console.log(x + " " + y);
                try {
                    if ((x >= 0) && (x <= buckX) && (y >= 0) && (y <= buckY)) {
                        if (canvas.walls.bucketss[x] != null && canvas.walls.bucketss[x][y] != null) {
                            walls = walls.concat(canvas.walls.bucketss[x][y]);
                            if (walls.length != 0) {

                                if (check_cols == true) {
                                    check_cols = false;
                                    let collision = WallsLayer.getWallCollisionsForRay(r, walls.filter(w => w && w.data.sense !== CONST.WALL_SENSE_TYPES.NONE && !((w.data.door > WALL_DOOR_TYPES.NONE) && (w.data.ds === WALL_DOOR_STATES.OPEN))), {
                                        mode: "closest"
                                    });

                                    if ((collision.x != r.B.x) || (collision.y != r.B.y) || (collision.t0 != 1) || (collision.t1 != 0)) {
                                        return collision;
                                    } else {
                                        //console.log('no');
                                        walls = [];
                                    }
                                }
                            }
                        }
                    }
                } catch { // console.log(x + " " + y);
                }
                //Only move in either X or Y coordinates, not both.
                if ((Math.abs(tMaxX) < Math.abs(tMaxY))) {
                    tMaxX += tDeltaX;
                    x += stepX;
                    if (finish_y){
                      check_cols = true;
                      finish_y=false;
                    }


                } else {
                    tMaxY += tDeltaY;
                    y += stepY;

                    if (!(Math.abs(tMaxX) < Math.abs(tMaxY))) {
                        finish_y = true;
                        // did x and y in total, now check

                    }
                }
            }

            let collision = WallsLayer.getWallCollisionsForRay(r, walls.filter(w => w && w.data.sense !== CONST.WALL_SENSE_TYPES.NONE && !((w.data.door > WALL_DOOR_TYPES.NONE) && (w.data.ds === WALL_DOOR_STATES.OPEN))), {
                mode: "closest"
            });

            if ((collision.x != r.B.x) || (collision.y != r.B.y) || (collision.t0 != 1) || (collision.t1 != 0)) {
                return collision;
            }

            return null;
            // collision = WallsLayer.getWallCollisionsForRay(r, [...new Set(walls)].filter(w => 
            // w && w.data.sense !== CONST.WALL_SENSE_TYPES.NONE && 
            // !((w.data.door > WALL_DOOR_TYPES.NONE) && (w.data.ds === WALL_DOOR_STATES.OPEN))), {mode: "closest"});

            // return null;
        }

        SightLayer.computeSight = (function(origin, radius, {
            minAngle = null,
            maxAngle = null,
            cullMin = 10,
            cullMult = 2,
            cullMax = 20,
            density = 6,
            walls,
            rotation = 0,
            angle = 360
        } = {}) {
            const cached = SightLayer.computeSight;
            return function(origin, radius, {
                minAngle = null,
                maxAngle = null,
                cullMin = 10,
                cullMult = 2,
                cullMax = 20,
                density = 6,
                walls,
                rotation = 0,
                angle = 360
            } = {}) {
                // Get the maximum sight distance and the limiting radius
                let d = canvas.dimensions;
                let {
                    x,
                    y
                } = origin;
                let distance = Math.max(d.width, d.height);
                let limit = radius / distance;
                let cullDistance = Math.clamped(radius * cullMult, cullMin * d.size, cullMax * d.size);
                angle = angle || 360;

                // Determine the direction of facing, the angle of vision, and the angles of boundary rays
                const limitAngle = angle.between(0, 360, false);
                const aMin = limitAngle ? normalizeRadians(toRadians(rotation + 90 - (angle / 2))) : -Math.PI;
                const aMax = limitAngle ? aMin + toRadians(angle) : Math.PI;

                // Cast sight rays needed to determine the polygons
                let rays = this._castSightRays(x, y, distance, cullDistance, density, limitAngle, aMin, aMax);

                // // Iterate over rays and record their points of collision with blocking walls
                //     if (radius < Math.min(canvas.dimensions.width,canvas.dimensions.height)){

                // //     let light_walls_path = [];
                // // let actor_lights = Array.from(game.scenes.active.data.tokens).filter(w => w && (w.dimLight != 0 ||w.brightLight != 0 )).map(
                // //     function(aa){return {l:{rad: aa.dimLight + aa.brightLight,y: aa.y,x: aa.x}}});

                // //    for ( let l of [].concat(game.scenes.active.data.lights , actor_lights)) {
                // //     let start = {x: Math.floor(origin.x/(canvas.dimensions.size*2)), y: Math.floor(origin.y/(canvas.dimensions.size*2))};
                // //     let end = {x: Math.floor(l.x/(canvas.dimensions.size*2)), y: Math.floor(l.y/(canvas.dimensions.size*2))};
                // //     light_walls_path = light_walls_path.concat(grid_intersect(start, end),getWallsByBuckets((Math.ceil(l.rad), l.x, l.y)));
                // //    // light_walls = light_walls.concat(this.getWallsByBuckets((Math.ceil(l.data.dim)+Math.ceil(l.data.bright))*10, l.data.x, l.data.y));
                // //     }

                //     let bucket_count = Math.ceil(radius/(canvas.dimensions.size*2));
                //     let bx = Math.floor(origin.x/(canvas.dimensions.size*2));
                //     let by = Math.floor(origin.y/(canvas.dimensions.size*2));
                //     walls = new Set(canvas.walls.bucketss.slice(bx-bucket_count, bx+bucket_count).map( 
                //       function(row){ return row.slice(by-bucket_count, by+bucket_count); }).flat(2).concat(light_walls_path).filter(w => 
                //       w && w.data.sense !== CONST.WALL_SENSE_TYPES.NONE && 
                //       !((w.data.door > WALL_DOOR_TYPES.NONE) && (w.data.ds === WALL_DOOR_STATES.OPEN) )));//.concat(canvas.lighting.light_walls).filter(w => w));
                // }
                // else{
                //   walls = canvas.walls.blockVision.filter(w => !((w.data.door > WALL_DOOR_TYPES.NONE) && (w.data.ds === WALL_DOOR_STATES.OPEN)));
                // }

                for (let r of rays) {

                    // Special case: the central ray in a limited angle
                    if (r._isCenter) {
                        r.unrestricted = r.limited = r.project(0.5);
                        continue;
                    }
                    // walls = Array.from(new Set([].concat())).filter(w=>w);
                    // Normal case: identify the closest collision point both unrestricted (LOS) and restricted (FOV)

                    // WallsLayer.getWallCollisionsForRay(r, walls, {
                    //     mode: "closest"
                    // });

                    let collision = grid_intersect(r);

                    r.unrestricted = collision || {
                        x: r.B.x,
                        y: r.B.y,
                        t0: 1,
                        t1: 0
                    };
                    r.limited = (r.unrestricted.t0 <= limit) ? r.unrestricted : r.project(limit);
                }

                // Reduce collisions and limits to line-of-sight and field-of-view polygons
                let [losPoints, fovPoints] = rays.reduce((acc, r) => {
                    acc[0].push(r.unrestricted.x, r.unrestricted.y);
                    acc[1].push(r.limited.x, r.limited.y);
                    return acc;
                }, [
                    [],
                    []
                ]);

                // Construct visibility polygons and return them with the rays
                const los = new PIXI.Polygon(...losPoints);
                const fov = new PIXI.Polygon(...fovPoints);
                return {
                    rays,
                    los,
                    fov
                };
                return cached.apply(this, arguments);
            };
        })();

        //         SightLayer.prototype.saveFog = ( function() {

        //             return  async function() {

        //     if ( !this.tokenVision || !this.fogExploration || !this._fogUpdated ) return;
        //     this._fogUpdated = false;
        //     let d = canvas.dimensions;

        //     // Use the existing rendered fog to create a Sprite and downsize to save with smaller footprint
        //     const fog = new PIXI.Sprite(this.fog.rendered.texture);
        //     let scl = d.width > 1920 ? 1920 / d.width : 1.0;
        //     fog.scale.set(scl, scl);

        //     // Add the fog to a temporary container to bound it's dimensions and export to base data
        //     const stage = new PIXI.Container();
        //     stage.addChild(fog);
        // // setTimeout(
        // // function(){
        //      // Update the fog data to store
        //     this.fogData.user = game.user._id;
        //     this.fogData.scene = canvas.scene._id;
        //     this.fogData.explored = canvas.app.renderer.extract.base64(stage);
        //     this.fogData.timestmp = Date.now();

        //     // Create or update the fog
        //     await this._createOrUpdateFogExploration(this.fogData);
        // //     stage.destroy({children: true});
        // // }

        // //   , 0);

        //             }
        //             ;
        //         }
        //         )();

        console.log('Walls - Hasted!!!');

    }

});

var HasteTimer;
var ismousedown = false;
var iscntrl = false;

function HasteSelf(duration = 2000) {
    if (ismousedown == false) {
        clearTimeout(HasteTimer);
        //canvas.app.ticker.maxFPS = 60;
        canvas.app.start()
        HasteTimer = window.setTimeout(function() {
            if (ismousedown == false) {
                canvas.app.ticker.maxFPS = 30;

                canvas.app.stop()
            }
        }, duration);
    }
}

Hooks.once('canvasReady', async function() {

    if (game.settings.get('Haste', 'enabledGPU')) {
        HasteSelf(3000);

        Hooks.on('hoverToken', async function() {
            HasteSelf();
        });
        Hooks.on('hoverNote', async function() {
            HasteSelf();
        });

        Hooks.on('hoverDoor', async function() {
            HasteSelf();
        });

        Hooks.on('createToken', async function() {
            HasteSelf();
        });
        Hooks.on('deleteActor', async function() {
            HasteSelf();
        });
        Hooks.on('deleteToken', async function() {
            HasteSelf();
        });
        Hooks.on('updateToken', async function() {
            HasteSelf();
        });
        Hooks.on('updateScene', async function() {
            HasteSelf();
        });
        Hooks.on('updateUser', async function() {
            HasteSelf();
        });
        Hooks.on('canvasPan', async function() {
            HasteSelf();
        });

        window.addEventListener("keyup", () => {
            HasteSelf()
        });

        window.addEventListener('mousedown', e => {
            ismousedown = true;
            iscntrl = e.ctrlKey;
            canvas.app.start()
        });
        window.addEventListener('mouseup', e => {
            ismousedown = false;
            if (iscntrl == false) {
                HasteSelf(1000);
            }
        });

        game.socket.on('modifyEmbeddedDocument', () => {
            HasteSelf()
        });
        game.socket.on('userActivity', () => {
            HasteSelf()
        });

        game.socket.on('HasteSelf', () => {
            HasteSelf()
        });

        console.log('GPU - Hasted!!!');

    }
});
